/*
* Platform Visualization - Client Tools for Class Tree
*
*----------------------------------------------------
*  Troy Pesola - troy@servicemanagementprof.com
*
*  http://servicemanagementprof.com/platform-vis
*----------------------------------------------------
*/
/*--------------------------------------------------------------------*/
/*--------------------------------------------------------------------*/
/*  JQuery and Prototype UI Fixes  */
/*--------------------------------------------------------------------*/
/*--------------------------------------------------------------------*/
if (Prototype.BrowserFeatures.ElementExtensions) {
  var disablePrototypeJS = function (method, pluginsToDisable) {
    var handler = function (event) {
      event.target[method] = undefined;
      setTimeout(function () { delete event.target[method];}, 0);
    };
    pluginsToDisable.each(function (plugin) {
      jQuery(window).on(method + '.bs.' + plugin, handler);
    });
  },
  pluginsToDisable = ['collapse', 'dropdown', 'modal', 'tooltip', 'popover'];
  disablePrototypeJS('show', pluginsToDisable);
  disablePrototypeJS('hide', pluginsToDisable);
}

/*--------------------------------------------------------------------*/
/*--------------------------------------------------------------------*/
/*  UI  */
/*--------------------------------------------------------------------*/
/*--------------------------------------------------------------------*/
/* create function to setup after scripts are loaded */
function pvis_setup() {
  var $j = jQuery.noConflict();
  /* --- setup the d3 Tree */
  x_0081_pvis.VisClassTree.Setup(graph,{});
  $j('#menuExpand').click(function(){ x_0081_pvis.VisClassTree.ExpandAll();});
  $j('#menuCollapse').click(function(){ x_0081_pvis.VisClassTree.CollapseAll();});
  $j('#menuSearchClr').click(function(){ x_0081_pvis.VisClassTree.SearchClear();});

  function pvis_search(t){
    var txt = $j('#pvis-search-text').val();
    var do_names = $j('#cbNames').is(':checked');
    var do_sub_names = $j('#cbSubNames').is(':checked');
    var do_labels = $j('#cbLabels').is(':checked');
    var do_fld_names = $j('#cbFldNames').is(':checked');
		var do_fld_labels = $j('#cbFldLabels').is(':checked');
		var do_fld_refs = $j('#cbFldRefs').is(':checked');
    var do_with_cnt = $j('#cbWithRecords').is(':checked');
    var do_inactiveflds = $j('#cbIncInactiveFlds').is(':checked');
    var do_createdby = $j('#cbCreatedBy').is(':checked');
    var do_updatedby = $j('#cbUpdatedBy').is(':checked');
    var do_updated_after = $j('#pvis-after-date').val();

    x_0081_pvis.VisClassTree.Search(txt, do_names, do_sub_names, do_labels,
			do_fld_names, do_fld_labels, do_fld_refs, do_with_cnt,
      do_inactiveflds, do_createdby, do_updatedby, do_updated_after);
	}
  $j('#pvis-search-btn').click(function(){ pvis_search('Search Button'); });
  $j('#pvis-search-text').keypress(function (e) {
    if (e.which == 13) {
      pvis_search('Search Enter');
      $j('.dropdown.open').removeClass('open');
      return false;
    }
  });
  $j('.dropdown-toggle').dropdown();
  $j('.dropdown-menu input, .dropdown-menu label').click(function(e){
  	if (e.stopPropagation) {/* normal browsers */e.stopPropagation();}
  	else if (window.event) { /* IE */window.event.cancelBubble = true;}
  });
}
ScriptLoader.getScripts([
  'x_0081_pvis.jquery v3.1.1.jsdbx',
  'x_0081_pvis.d3.v4.min.jsdbx',
  'x_0081_pvis.bootstrap.min.jsdbx',
  'x_0081_pvis.VisClassTree.jsdbx'
], function(){
  if (document.readyState === "complete" ||
  (document.readyState !== "loading" && !document.documentElement.doScroll)) {
    pvis_setup();
  } else {
    document.addEventListener("DOMContentLoaded", pvis_setup);
  }
});
