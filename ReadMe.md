# ServiceNow Platform Visualization

> Platform Visualization of Data tables and Process Flows.

This project implements a means to visualize the data structures and process flows in ServiceNow that come from either task states or workflow stages.

This project contains the code modules, plus the wiki and issue tracker for the effort of adding process views into the application.

The ServiceNow App Studio repository for the Platform Visualization Scoped application is located at GitHub [Platform Visualization](https://github.com/troypesola/servicenow-platformvisualization).  The main script files are replicated here.

The issue tracker in the main repo should be used for ongoing issues.

Use the [Wiki](wiki) for documentation.

## Notes

* Data extensions and references is working
* Process Flow is under development
* 1.2 fixed the changes in sys_dictionary due to ServiceNow replicating all inherited fields in cmdb extended tables
* GitHub repo used for the app due to ServiceNow Rome release not authenticating with bitbucket.