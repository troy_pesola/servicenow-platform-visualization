/*---------------------------------------------------
* VisStateFlow
*   Visualization for task state process flows.
*
*----------------------------------------------------
*  Troy Pesola - troy@servicemanagementprof.com
*
*  http://servicemanagementprof.com/platform-vis
*----------------------------------------------------
* Usage:
*   var vsf = new VisStateFlow();
*
* vsf.analyze()
*  - run the analysis on the state-based tables
*  - to determine all the tables based off task
*  - find the default states for work, and closed
*  - also determine how many records exist of each
*  - type of record.
*
* var t = vsf.getTasks()
*  - get an associative array of task objects
*  - keys are task table names, with each containing:
*     -label = table class Label
*     -name = table class name
*     -desc = optional description for the class
*     -records = number of records
*     -work = value for the default work state
*     -closed = value for the default closed state
*     -closed_states = array of closed states
*     -states: array of states in sequency order
*       -value = state value
*       -label = state label
*       -class_str = string of classes for the state
*
*/
var VisStateFlow = Class.create();
VisStateFlow.prototype = Object.extendsObject(VisBaseFlow, {

  analyze: function() {
    this.tasks = {};
    this._do_task_type('task');
    this._get_counts();
  },

  /*-----------------------------------------------------------------------*/
  /*-----------------------------------------------------------------------*/
  /* Private Object Functions */
  /*-----------------------------------------------------------------------*/
  /*-----------------------------------------------------------------------*/
  _do_task_type: function(/*string */class_name) {
    /* get the summary information */
    var grdb = new GlideRecord('sys_db_object');
    if (!grdb.get('name',class_name)) return;

    /* add this class to the task array */
    this.tasks[class_name]={};
    this.tasks[class_name].name = ''+grdb.name;
    this.tasks[class_name].label = ''+grdb.label;
    this.tasks[class_name].desc = '';
    this.tasks[class_name].records = 0;
    this._get_state_attributes(class_name);
    this._get_state_choices(class_name);

    /* recurse through the task classes extended from this class */
    var grdbe = new GlideRecord('sys_db_object');
    grdbe.addQuery('super_class',grdb.sys_id);
    grdbe.query();
    while (grdbe.next()) {
      this._do_task_type(grdbe.name);
    }
  },

  _get_counts: function() {
    /* get the total count */
    var grc = new GlideAggregate('task');
    grc.addQuery('sys_class_name','IN',Object.keys(this.tasks).join(','));
    grc.addAggregate('COUNT');
    grc.query();
    if (grc.next()) {
      this.tasks['task'].records = grc.getAggregate('COUNT');
    }

    /* get the count of each type */
    grc = new GlideAggregate('task');
    grc.addQuery('sys_class_name','IN',Object.keys(this.tasks).join(','));
    grc.addAggregate('COUNT','sys_class_name');
    grc.query();
    while (grc.next()) {
      if (grc.sys_class_name=='') {
        gs.print('TRP empty aggregate grc='+JSON.stringify(grc));
        continue;
      }
      var c=''+grc.sys_class_name;
      this.tasks[c].records = grc.getAggregate('COUNT','sys_class_name');
    }
  },

  _get_state_attributes: function(/*string */class_name) {
    /*  - from TaskStateUtil Script Include -
    * SYSTEM_DEFAULT_CLOSE : 3, // task closed complete state
    * SYSTEM_DEFAULT_WORK : 2, // task work in progress state
    * SYSTEM_INACTIVE_STATES : [3, 4, 7], // task default inactive/close states
    */
    this.tasks[class_name].default_work = 2;
    this.tasks[class_name].default_closed = 3;
    this.tasks[class_name].closed_states = [3,4,7];

    var grdo = new GlideRecord('sys_dictionary_override');
    grdo.addQuery('element','state');
    grdo.addQuery('name',class_name);
    grdo.addNotNullQuery('attributes');
    grdo.query();
    if (grdo.next()) {
      var attr = grdo.attributes.split(',');
      for (var x=0; x<attr.length; x++) {
        var attr_arr = attr[x].split('=');
        if (attr_arr.length >= 2) {
          /* pull the key and value */
          var k=attr_arr[0].toLowerCase();
          if (k=='default_work_state') {
            this.tasks[class_name].default_work = attr_arr[1];
          } else if (k=='default_close_state') {
            this.tasks[class_name].default_closed = attr_arr[1];
          } else if (k=='close_states') {
            this.tasks[class_name].closed_states = attr_arr[1].split(';');
          }
        }
      }
    }
  },

  /* get state choices
  * k = key in the tasks array
  * c = class name to use, traverse up the parents until found
  */
  _get_state_choices: function(/*string */k,/*string */c){
    c = c||k;

    this.tasks[k].states=[];
    var grch = new GlideRecord('sys_choice');
    grch.addQuery('name',c);
    grch.addQuery('element','state');
    grch.addQuery('inactive',false);
    grch.orderBy('sequence');
    grch.query();
    var s = 0;
    while (grch.next()) {
      this.tasks[k].states[s]={};
      this.tasks[k].states[s].label = ''+grch.label;
      this.tasks[k].states[s].value = ''+grch.value;
      this.tasks[k].states[s].class_str = this._get_class(k,''+grch.value);
      s++;
    }
    this.tasks[k].state_cnt=s;
    if (!s) {
      /* find the parent and try again */
      var grdb = new GlideRecord('sys_db_object');
      if (grdb.get('name',c)) {
        this._get_state_choices(k,''+grdb.super_class.name);
      }
    }
  },

  _get_class: function(/*string task */t, /*state value*/v) {
      var class_str='',classes=[];

      if (v==this.tasks[t].default_work) {
        classes.push('pvis_default_work');
      } else if (v==this.tasks[t].default_closed) {
        classes.push('pvis_default_closed');
      }

      if (this._is_closed_state(t,v)) { classes.push('pvis_closed'); }

      if (classes.length) class_str = classes.join(' ');
      return class_str;
  },

  _is_closed_state: function(/*string task*/t, /*state value*/v) {
    var rval=false;
    for (var i = 0, len = this.tasks[t].closed_states.length; i < len; i++) {
      if (v==this.tasks[t].closed_states[i]) {
        rval=true;
        break;
      }
    }
    return rval;
  },

  type: 'VisStateFlow'
});
