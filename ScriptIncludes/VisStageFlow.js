/*---------------------------------------------------
* VisStageFlow
*   Visualization for workflow stages
*
*----------------------------------------------------
*  Troy Pesola - troy@servicemanagementprof.com
*
*  http://servicemanagementprof.com/platform-vis
*----------------------------------------------------
* Usage:
*   var vsf = new VisStageFlow();
*
* vsf.analysze()
*  - run the analysis on the workflow stages
*  - to determine all the sets of stages used
*  - also determine how many records exist of each
*  - type of r.
*
* vsf.dump()
*  - print the data with gs.print()
*
* var t = vsf.getStages()
*  - get an associative array of stage objects
*  - keys are task table names, with each containing:
*     -label = 'Stage Set'
*     -name = same as key, number of stages plus an alpha
*     -desc = text list of workflow version using this stage set
*     -records = number of records (na)
*     -statecnt = number of stages
*     -states = array of stage objects
*        -value = stage value
*        -label = stage label
*        -class_str = string of classes for the stage
*     -work = n/a
*     -closed = n/a
*     -closed_states = n/a
*
* var keys = vsf.getStageKeys()
*  - get an array of the keys for the task objects
*
*
*/
var VisStageFlow = Class.create();
VisStageFlow.prototype = Object.extendsObject(VisBaseFlow, {

  analyze: function() {
    this.tasks = {};
    this._do_stages();
  },

  /*-----------------------------------------------------------------------*/
  /*-----------------------------------------------------------------------*/
  /* Private Object Functions */
  /*-----------------------------------------------------------------------*/
  /*-----------------------------------------------------------------------*/
  _do_stages: function() {
    /* get the stages
    *  -sys choices for table
    *  -wf_stage_default for table
    */
    var self=this;

    /* find all of the published and active workflow version */
    /* then find the associated stages */
    /* add to the array based on the number of stages found */
    var gr_wfv = new GlideRecord('wf_workflow_version');
    gr_wfv.addQuery('published',true);
    gr_wfv.addQuery('active',true);
    gr_wfv.addQuery('table','sc_req_item');
    gr_wfv.query();
    while (gr_wfv.next()) {
      /* add the stages
      *  -wf_stage for this wf version
      */
      var stages = {};
      var s = 0;
      var gr_wfs = new GlideRecord('wf_stage');
      gr_wfs.addQuery('workflow_version',gr_wfv.sys_id);
      gr_wfs.orderBy('order');
      gr_wfs.query();
      //gs.print('wf: '+gr_wfv.name);
      while (gr_wfs.next()) {
        //gs.print('  '+gr_wfs.name+'('+gr_wfs.value+') seq='+gr_wfs.order);
        var sv=''+gr_wfs.value;
        if (typeof stages[sv] === 'undefined') {
          stages[sv]={};
          stages[sv].value = sv;
          stages[sv].label = ''+gr_wfs.name;
          stages[sv].sequence=''+gr_wfs.order;
          stages[sv].class_str='';
          s++;
        }
      }

      /* build a sorted array of the stage values */
      keysSorted = Object.keys(stages).sort(function(a,b){
        return stages[a].sequence-stages[b].sequence;
      });
      var skey='';
      keysSorted.forEach(function(i,t){
        var sv=''+stages[i].value;
        skey+=sv.toLowerCase().replace(/\W/g, '');
        //gs.print('sv='+sv+'  skey='+skey);
      });
      //gs.print('skey:'+skey);

      /* see if this is a new stage set */
      //if (!(skey in this.tasks)) {
      if (!(this.tasks.hasOwnProperty(skey))) {
        /* new stage set */
        this.tasks[skey] = {};
        this.tasks[skey].name = s;
        this.tasks[skey].skey = skey;
        this.tasks[skey].label = 'Stage Set';
        this.tasks[skey].found=true;
        this.tasks[skey].desc = ''+gr_wfv.name;
        this.tasks[skey].states={};
        this.tasks[skey].statecnt=0;
        keysSorted.forEach(function(i,t){
          var sc = self.tasks[skey].statecnt;
          self.tasks[skey].statecnt++;
          self.tasks[skey].states[sc]=self._clone(stages[i]);
        });
        this.tasks[skey].records = 1;
        this.tasks[skey].default_closed = '';
        this.tasks[skey].default_work = '';
        this.tasks[skey].closed_states = [];
      } else {
        /* add to an existing stage set */
        this.tasks[skey].desc += ', '+gr_wfv.name;
        this.tasks[skey].records++;
      }
    }

    /* now redo the stage sequence keys */
    keysSorted = Object.keys(this.tasks).sort(function(a,b){
      return self.tasks[a].statecnt-self.tasks[b].statecnt;
    });
    var skey='';
    for (var key in keysSorted) {
      var n=0;
      var idx=keysSorted[key];
      var idx2=keysSorted[key-n-1];
      while (key>0 && ((key-n-1)>=0) &&
        (this.tasks[idx].statecnt==this.tasks[idx2].statecnt)) {
        n++;
        idx2=keysSorted[key-n-1];
      }
      var newkey = 's'+this._stageStr(this.tasks[idx].statecnt,n);
      this.tasks[idx].name=newkey;
    }
    /* now finally loop through and redo the keys to something usable */
    for (var key in this.tasks) {
      var i=this.tasks[key].name;
      this.tasks[i]=this._clone(this.tasks[key]);
      delete this.tasks[key];
    }
  },

  _clone: function(/* object */obj) {
    return JSON.parse(JSON.stringify(obj));
  },

  _stageStr: function(/*string base name*/b,/*int number to add*/n) {
    var str=b;
    if (n>0){
      var c='a';
      str = b + String.fromCharCode(c.charCodeAt(0) + (n-1));
    }
    return str;
  },

  type: 'VisStageFlow'
});
