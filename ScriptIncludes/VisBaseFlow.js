/*---------------------------------------------------
* VisBaseFlow
*   Base class for process flow visualization.
*   Used for both Task States and Workflow stages
*
*----------------------------------------------------
*  Troy Pesola - troy@servicemanagementprof.com
*
*  http://servicemanagementprof.com/platform-vis
*----------------------------------------------------
*
*/
var VisBaseFlow = Class.create();
VisBaseFlow.prototype = {
  tasks: {},
  initialize: function() {
    this.tasks = {};
  },

  getTasks: function() { return this.tasks; },
  getKeys: function() { return Object.keys(this.tasks); },

  getKeysAlpha: function() {
    var skeys=[];
    var self=this;
    var keys = Object.keys(this.tasks);
    skeys = keys.sort(function(a,b){
      var al = self.tasks[a].label;
      var bl = self.tasks[b].label;
      return al.toLowerCase().localeCompare(bl.toLowerCase());
    });
    /* move task to the front */
    var toIndex=0;
    var fromIndex=skeys.indexOf('task');
    skeys.splice(toIndex, 0, keys.splice(fromIndex, 1)[0] );
    return skeys;
  },


  dump: function() {
    var dump_msg='';
    dump_msg +='\n\n\n DUMP \n\n\n';
    for (var key in this.tasks) {
      var msg="\n\n" + key + ' ' + this.tasks[key].label + '('+ this.tasks[key].name+')';
      msg += ' records='+this.tasks[key].records;
      msg += ' work='+this.tasks[key].default_work;
      msg += ' closed='+this.tasks[key].default_closed;
      msg += ' closed_states='+this.tasks[key].closed_states.join(',');
      msg += '\n  desc=' + this.tasks[key].desc;
      msg += '\n  states  cnt='+this.tasks[key].state_cnt;
      for (var s=0; s<this.tasks[key].state_cnt;s++){
        msg +='\n   '+this.tasks[key].states[s].label+'('+this.tasks[key].states[s].value+')';
      }
      dump_msg += '\n'+ msg + '\n';
    }
    return dump_msg;
  },


  /*-----------------------------------------------------------------------*/
  /*-----------------------------------------------------------------------*/
  /* Private Object Functions */
  /*-----------------------------------------------------------------------*/
  /*-----------------------------------------------------------------------*/


  type: 'VisBaseFlow'
};
